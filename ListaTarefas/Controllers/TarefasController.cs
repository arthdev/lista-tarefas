﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ListaTarefas.Contexts;
using ListaTarefas.Models;
using ListaTarefas.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ListaTarefas.Controllers
{
    [Route("api/[controller]")]
    // Define que o controller exige a política de autorizacao abaixo
    [Authorize(AuthorizationPolicies.ADULTO)]
    public class TarefasController : Controller
    {
        private ListaTarefasContext context;

        public TarefasController(ListaTarefasContext context)
        {
            this.context = context;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(context.Tarefas
                      .Include((c) => c.Responsavel)
                .ToList());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var tarefa = context.Tarefas
                .Include((c) => c.Responsavel)
                .Where((c) => c.Id == id)
                .Single();
            
            if (tarefa == null) {
                return NotFound();
            }

            return Ok(tarefa);
        }

        // POST api/values
        [HttpPost]
        [TypeFilter(typeof(ValidationAttribute))]
        public IActionResult Post([FromBody]Tarefa model)
        {
            if (ModelState.IsValid && false == true) {
                using (context) {
                    context.Add(model);
                    context.SaveChanges();
                }
                return Ok(model);
            }
            return BadRequest(ModelState);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        // aplicando um FilterAttribute cujo construtor precisa de parametros vindos da DI do App
        [TypeFilter(typeof(ValidationAttribute))]
        public IActionResult Put(int id, [FromBody]Tarefa model)
        {
            if (ModelState.IsValid) {
                using (context) {
                    var tarefa = context.Tarefas.Find(id);
                    if (tarefa == null) {
                        return NotFound();
                    }
                    tarefa.Descricao = model.Descricao;
                    tarefa.ResponsavelId = model.ResponsavelId;
                    context.SaveChanges();
                    return Ok(tarefa);
                }
            }
            return BadRequest(ModelState);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
