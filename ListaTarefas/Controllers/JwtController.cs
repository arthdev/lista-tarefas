﻿using System;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ListaTarefas.Controllers
{
    // Controller que fornece JWTs
    [Route("api/[controller]")]
    public class JwtController : Controller
    {
        private readonly UsuarioRepository repository;
        private readonly TokenConfigurations tokenConfig;
        private readonly SigningConfigurations signingConfig;

        public JwtController(UsuarioRepository usuarioRepository,
            TokenConfigurations tokenConfigurations,
            SigningConfigurations signingConfigurations
        ) {
            this.repository = usuarioRepository;
            this.tokenConfig = tokenConfigurations;
            this.signingConfig = signingConfigurations;
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Post([FromBody]Login login)
        {
            var usuario = repository.Buscar(login.Nome);
            if (usuario != null && usuario.Senha == login.Senha)
            {
                DateTime dataCriacao = DateTime.Now;
                DateTime dataExpiracao = dataCriacao.AddSeconds(tokenConfig.Seconds);

                var handler = new JwtSecurityTokenHandler();
                var jwtSecurityToken = handler.CreateToken(new SecurityTokenDescriptor()
                {
                    Issuer = tokenConfig.Issuer,
                    Audience = tokenConfig.Audience,
                    IssuedAt = dataCriacao,
                    Expires = dataExpiracao,
                    NotBefore = dataCriacao,
                    SigningCredentials = signingConfig.SigningCredentials,
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, usuario.Nome),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Birthdate, usuario.DataNascimento.ToString(new CultureInfo("pt-BR"))),
                    })
                });

                var token = handler.WriteToken(jwtSecurityToken);
                return Ok(token);
            }
            return Unauthorized();
        }
    }
}
