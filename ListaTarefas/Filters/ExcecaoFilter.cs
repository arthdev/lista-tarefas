﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ListaTarefas.Filters
{
    // Filters são usados para executar operacoes antes ou depois de algum momento no pipeline da requisicao
    public class ExcecaoFilter : IExceptionFilter
    {
        public ExcecaoFilter()
        {

        }

        void IExceptionFilter.OnException(ExceptionContext context)
        {
            context.Result = new ObjectResult(new { mensagem = "Ocorreu um erro inesperado." })
            {
                StatusCode = 500
            };
        }
    }
}
