﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Localization;

namespace ListaTarefas.Filters
{
    // FilterAttribute são utilizados para adicionar um Filter a alguma action ou controller específico
    public class ValidationAttribute : ActionFilterAttribute
    {
        private IStringLocalizer<SharedResource> localizer;

        // Parametros do construtor do FilterAttribute que vem dos services da app são fornecidos aplicando 
        // TypeFilterAttribute passando como parametro Type o tipo do FilterAttribute
        public ValidationAttribute(IStringLocalizer<SharedResource> localizer)
        {
            this.localizer = localizer;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {}

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var errors = new Dictionary<string, string[]>();
                var keys = context.ModelState.Keys;
                foreach (var key in keys)
                {
                    if (context.ModelState[key].ValidationState == ModelValidationState.Invalid) {
                        var keyErrors = context.ModelState[key].Errors
                           .Select((e) => string.IsNullOrEmpty(e.ErrorMessage) ? localizer["Formato inválido."] : e.ErrorMessage)
                           .ToArray();
                        errors.Add(key, keyErrors);
                    }
                }
                context.Result = new BadRequestObjectResult(errors);
            }
        }
    }
}
