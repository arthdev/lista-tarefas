﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using ListaTarefas.Contexts;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using ListaTarefas.Filters;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using System;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.HttpOverrides;
using System.Net;

namespace ListaTarefas
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<UsuarioRepository>();

            // EF Core
            string connectionString = Configuration.GetConnectionString("APP");
            services.AddDbContext<ListaTarefasContext>((opts) => opts.UseNpgsql(connectionString));

            // Path das resources files para localization
            services.AddLocalization(o =>
            {
                o.ResourcesPath = "Resources";
            });

            // Authenticacao com JWTs
            // Busca Configuracoes de assinatura do token
            var signingConfigutations = new SigningConfigurations();
            services.AddSingleton(signingConfigutations);

            // Busca Configuracoes do token
            var tokenConfigurations = new TokenConfigurations();
            new ConfigureFromConfigurationOptions<TokenConfigurations>(Configuration.GetSection("TokenConfigurations"))
                .Configure(tokenConfigurations);
            services.AddSingleton(tokenConfigurations);

            // Especifica Validacoes para os JWTs
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(opt =>
                {
                    var validationParams = opt.TokenValidationParameters;
                    validationParams.ValidateAudience = true;
                    validationParams.ValidateIssuer = true;
                    validationParams.ValidateLifetime = true;
                    validationParams.ValidateIssuerSigningKey = true;
                    validationParams.ValidAudience = tokenConfigurations.Audience;
                    validationParams.ValidIssuer = tokenConfigurations.Issuer;
                    validationParams.IssuerSigningKey = signingConfigutations.Key;
                });

            // Adiciona Politicas de authorizacao que requerem autenticacao com JWTs
            services.AddAuthorization(auth =>
            {
                auth.AddPolicy(AuthorizationPolicies.BEARER, new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .Build());

                auth.AddPolicy(AuthorizationPolicies.ADULTO, new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .RequireAssertion((u) =>
                        u.User.HasClaim((c) => c.Type.Equals(ClaimTypes.DateOfBirth) &&
                        DateTime.Parse(c.Value, new CultureInfo("pt-BR")) <= DateTimeOffset.Now.AddYears(-18)))
                    .Build());
            });

            // Adiciona CORS - Cross Origin Resource Sharing
            services.AddCors((opt) =>
            {
                // Adiciona Politica do CORS
                opt.AddPolicy(CorsPolicies.API, (builder) =>
                {
                    builder.WithMethods(HttpMethods.Put, HttpMethods.Delete, HttpMethods.Post, HttpMethods.Get)
                        .WithOrigins("http://localhost:8080")
                        .AllowAnyHeader()
                        .Build();
                });
            });

            services.AddMvc((o) =>
            {
                // Adiciona Filters globais
                o.Filters.Add<ExcecaoFilter>();
            })

            // Especifica Resoruce File unificado para Data Annotations
            .AddDataAnnotationsLocalization(o =>
            {
                o.DataAnnotationLocalizerProvider = (type, factory) =>
                {
                    return factory.Create(typeof(SharedResource));
                };
            })

            // Habilitar requests e reponses em XML
            .AddXmlSerializerFormatters();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Cullturas suportadas para locazalization da app
            IList<CultureInfo> supportedCultures = new List<CultureInfo>
            {
                new CultureInfo("pt-BR"),
                new CultureInfo("hr-HR"),
            };
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("pt-BR"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });

            // Define que usurá o esquema de autenticacao configurada anteriormente
            app.UseAuthentication();

            // Define que usurá a política do CORS especifcada em toda a aplicacao
            app.UseCors(CorsPolicies.API);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
