﻿using System;
using System.Security.Cryptography;
using Microsoft.IdentityModel.Tokens;

namespace ListaTarefas
{
    public class SigningConfigurations
    {
        public SecurityKey Key { get; }
        public SigningCredentials SigningCredentials { get; }

        public SigningConfigurations()
        {
            byte[] bytes = new byte[32];
            RNGCryptoServiceProvider rNGCryptoServiceProvider = new RNGCryptoServiceProvider();
            rNGCryptoServiceProvider.GetBytes(bytes);
            Key = new SymmetricSecurityKey(bytes);
            SigningCredentials = new SigningCredentials(Key, SecurityAlgorithms.HmacSha256);
        }
    }
}
