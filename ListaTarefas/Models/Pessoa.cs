﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ListaTarefas.Models
{
    public class Pessoa
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
