﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ListaTarefas.Models;

namespace ListaTarefas.Models
{
    public class Tarefa
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório.")]
        public string Descricao { get; set; }

        // Importante:
        // se a chave for de tipo não nullable, então a coluna será not null, e o relacionamento, obrigatório

        // se a chave estrangeira for de tipo nullable, então a coluna no banco será nullable, 
        // e o relacionamento será opcional, a menos que seja configurado no FluntApi como required 
        // (IsRequired no relaciomaneto)
        [Required(ErrorMessage = "{0} é obrigatório.")]
        public int? ResponsavelId { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório.")]
        public DateTime? DataFimPrazo { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório.")]
        public decimal? Nota { get; set; }

        public Pessoa Responsavel { get; set; }
    }
}
