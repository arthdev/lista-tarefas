﻿using System;
using ListaTarefas.Models;
using Microsoft.EntityFrameworkCore;

namespace ListaTarefas.Contexts
{
    public class ListaTarefasContext : DbContext
    {
        public DbSet<Tarefa> Tarefas
        {
            get;
            set;
        }

        public DbSet<Pessoa> Pessoas
        {
            get;
            set;
        }

        public ListaTarefasContext(DbContextOptions<ListaTarefasContext> options) : base (options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tarefa>()
                        .HasOne<Pessoa>((c) => c.Responsavel)
                        .WithMany()
                        .HasForeignKey((c) => c.ResponsavelId)
                        .IsRequired()
                        .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Tarefa>()
                        .Property((c) => c.Descricao)
                        .IsRequired()
                        .HasMaxLength(255);

            modelBuilder.Entity<Tarefa>()
                        .Property((t) => t.Nota)
                        .IsRequired();

            modelBuilder.Entity<Tarefa>()
                        .Property((t) => t.DataFimPrazo)
                        .IsRequired();

            modelBuilder.Entity<Pessoa>()
                        .Property((p) => p.Nome)
                        .IsRequired()
                        .HasMaxLength(255);

            modelBuilder.Entity<Tarefa>()
                        .ToTable<Tarefa>("tarefas");

            modelBuilder.Entity<Pessoa>()
                        .ToTable<Pessoa>("pessoas");

            base.OnModelCreating(modelBuilder);
        }
    }
}
