﻿using System;
using System.Collections.Generic;
using System.Linq;
using ListaTarefas.Models;

namespace ListaTarefas
{
    public class UsuarioRepository
    {
        private static List<Usuario> usuarios = new List<Usuario>() {
            new Usuario()
            {
                Nome = "Arthur",
                Senha = "123456",
                DataNascimento = new DateTime(1995, 08, 27, 8, 52, 00)
            },
            new Usuario()
            {
                Nome = "Cristina",
                Senha = "crcsc",
                DataNascimento = new DateTime(1996, 04, 16 )
            },
            new Usuario() {
                Nome = "Luisa",
                Senha = "ufxc",
                DataNascimento = new DateTime(2000, 12, 08)
            }
        };

        public Usuario Buscar(string nome)
        {
            return usuarios.SingleOrDefault(u => u.Nome.Equals(nome) || u.Nome == nome);
        }
    }
}
